# Pega Yext



## Overview

Pega Yext is a custom Drupal integration of [Yext Answers Search UI SDK](https://hitchhikers.yext.com/docs/answers-sdk/).

This module allows to embed Yext Search UI on a Drupal website as a block.

## Prerequisites

- [ ] In order to use this module, you need a [Yext account](https://yext.com) with Answers experience configured.

## Installation

- [ ] Download the module using composer `composer require pega/pega_yext`
- [ ] Enable the module `drush pm:enable pega_yext`

## Configuration

- [ ] Copy variables from `pega_yext.services.yml` to `config/services/<environment>.yml` and set them accordingly.
- [ ] Each vertical (tab) requires it's own page/node with specific URL address, so you'll have to create a node for each one with `Pega Yext Block` embedded in a content region.
- [ ] URL for each vertical can be overridden in `config/services/<environment>.yml`, but it must match node's URL alias.
- [ ] In the Drupal UI, go to Structure > Block Layout, find "Pega Yext" block and place it in a "Content" region.
- [ ] Limit block visibility only to pages defined in `pega_yext.services.yml`.
- [ ] Alternatively, you can render the block from a custom code by using it's id: `pega_yext_search_block` or embed a block to the node itself.

