const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const eslintConfig = require('./.eslintrc.json');
const babelOptions = require('./.babelrc.json');

const config = {
  entry: {
    'vertical-search': './src/',
  },
  devtool: process.env.NODE_ENV === 'production' ? false : 'inline-source-map',
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  devServer: {
    open: true,
    client: {
      overlay: false,
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          // Including 'pega-react' JS requires explicitly setting babel options here
          options: babelOptions,
        },
      },
      {
        test: /\.(scss|css)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'public/lib', to: 'lib' }],
    }),
    new ESLintPlugin({
      failOnError: false,
      baseConfig: eslintConfig,
      useEslintrc: false,
    }),
    new MiniCssExtractPlugin(),
  ],
};

module.exports = config;
