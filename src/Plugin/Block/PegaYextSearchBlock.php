<?php

namespace Drupal\pega_yext\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Pega Yext Search block.
 *
 * @Block(
 *   id = "pega_yext_search_block",
 *   admin_label = @Translation("Pega Yext Search block"),
 *   category = @Translation("Pega")
 * )
 */
class PegaYextSearchBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Form Builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * Database Connection property.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Builds a new Dynamic Content block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = \Drupal::request()->getRequestUri();
    $drupal_container = \Drupal::getContainer();
    $renderable = [];

    switch (TRUE) {
      case stristr($url, $drupal_container->getParameter('pega_yext.tab_all_url')):
        $renderable = [
          '#theme' => 'pega_yext_all',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_docs_url')):
        $renderable = [
          '#theme' => 'pega_yext_documentation',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_training_url')):
        $renderable = [
          '#theme' => 'pega_yext_training',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_support_url')):
        $renderable = [
          '#theme' => 'pega_yext_support',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_knowledge_panel_url')):
        $renderable = [
          '#theme' => 'pega_yext_knowledge_panel',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_marketplace_url')):
        $renderable = [
          '#theme' => 'pega_yext_marketplace',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_videos_url')):
        $renderable = [
          '#theme' => 'pega_yext_videos',
        ];
        break;

      case stristr($url, $drupal_container->getParameter('pega_yext.tab_blogs_url')):
        $renderable = [
          '#theme' => 'pega_yext_blogs',
        ];
        break;
    }

    $renderable['#attached']['library'][] = 'pega_yext/vertical_search';
    $renderable['#attached']['library'][] = 'pega_bolt_video/brightcove-integration';

    return $renderable;
  }

}
