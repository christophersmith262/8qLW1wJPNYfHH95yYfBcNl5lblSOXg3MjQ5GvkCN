<?php
namespace Drupal\pega_yext\Normalizer;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\TypedData\Plugin\DataType\StringData;
use Drupal\pega_cd_reference\Plugin\Field\FieldType\EcmEntityReferenceItem;
use Drupal\serialization\Normalizer\TypedDataNormalizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class JSONNormalizerClass extends TypedDataNormalizer {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * TaxonomyService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   EntityTypeManager.
   * @param \Drupal\Core\Database\Connection
   *   DB Service.
   */
  public function __construct(EntityTypeManager $entityTypeManager, Connection $database) {
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  public function getTaxonomyStorage() {
    return $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * Filter out ECM term ID from the JSON output for further processing.
   *
   * @param object $entity
   *   Entity object.
   * @param $data
   *
   * @return bool|string
   *   Vocabulary type (string) or FALSE if there is no match.
   */
  private function verifyEcmTerm($entity, $data) {

    if ($entity instanceof StringData && ($entity->getParent() instanceof StringItem || $entity->getParent() instanceof EcmEntityReferenceItem)) {

      if (preg_match("/Prd/i", $data) || preg_match("/HorPrd/i", $data)) {
        return 'product';
      }

      if (preg_match("/PlatCap/i", $data) || preg_match("/PCSub/i", $data)) {
        return 'capability';
      }

      if (preg_match("/Role/i", $data)) {
        return 'role';
      }

      if (preg_match("/Ind/i", $data)) {
        return 'industry';
      }

      if (preg_match("/RConTyp/i", $data)) {
        return 'content_type';
      }

      return FALSE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    $data = parent::normalize($entity, $format, $context);

    $vocab_type = $this->verifyEcmTerm($entity, $data);

    if ($vocab_type) {
      $query = $this->database->select('taxonomy_term__field_pega_cd_id', 'ttfpci');
      $query->fields('ttfpci', ['field_pega_cd_id_value']);
      $query->leftJoin('taxonomy_term_field_data', 'ttfd', 'ttfpci.entity_id = ttfd.tid');
      $query->fields('ttfd', ['name', 'vid', 'tid']);
      $query->leftJoin('taxonomy_term__parent', 'ttp', 'ttp.entity_id = ttfd.tid');
      $query->fields('ttp', ['parent_target_id']);
      $query->condition('ttfpci.field_pega_cd_id_value', $data, '=');
      $query->condition('ttfpci.langcode', 'en', '=');
      $query->condition('ttfd.status', '1', '=');
      $query->condition('ttfd.langcode', 'en', '=');

      // Execute query, and return fetched results.
      $rows = $query->execute()->fetchAll();
      $versions = [];
      $taxonomy_storage = $this->getTaxonomyStorage();
      foreach ($rows as $row) {

        $parent = $taxonomy_storage->load($row->parent_target_id);
        if (!is_null($parent)) {
          $parent = $parent->label();
        }

        $versions['ecm_id'] = $row->field_pega_cd_id_value; // PrdVer321 [Product / ecm_term_id]

        switch ($vocab_type) {
          case 'product':
            $versions['parent_term_name'] = $row->name; // Pega Warranty [Product Name / parent_term_name]
            if (floatval($row->name) && !is_null($parent)) {
              $versions['product_version'] = $row->name; // 8.5 [Product Version / product_version]
              $versions['parent_term_name'] = $parent; // Pega Warranty [Product Name / parent_term_name]
            }
            break;

          case 'capability':
            $versions['capability'] = $row->name;
            if (!is_null($parent)) {
              $versions['sub_capability'] = $row->name;
              $versions['capability'] = $parent;
              $versions['capability_hierarchical'] = $parent . ' > ' . $row->name;
            }
            break;

          case 'industry':
            $versions['industry'] = $row->name;
            break;

          case 'role':
            $versions['role'] = $row->name;
            break;
        }
      }

      if (!empty($versions)) {
        return $versions;
      }
    }

    return $data;
  }
}