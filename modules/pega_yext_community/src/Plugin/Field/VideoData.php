<?php

namespace Drupal\pega_yext_community\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\FileInterface;
use Drupal\node\NodeInterface;
use Drupal\file\Entity\File;

/**
 * Create new video_data field for Video Content Type.
 */
class VideoData extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Computes the field value.
   */
  protected function computeValue() {
    $video_data = [];
    $video_node = $this->getEntity();
    if ($video_node instanceof NodeInterface) {
      if ($video_node->hasField('field_brightcove') && !$video_node->field_brightcove->isEmpty() && $video_node->field_brightcove->entity) {
        if (method_exists($video_node->field_brightcove->entity, 'getBrightcoveId')) {
          $video_id = $video_node->field_brightcove->entity->getBrightcoveId();
        }
        else {
          $video_id = $video_node->field_brightcove->entity->getVideoId();
        }
        $created = date('M d, Y', $video_node->field_brightcove->entity->created->value);
        $alt = $video_node->field_brightcove->entity->getName();
        $video_data = [
          'videoId' => $video_id,
          'created' => $created,
          'alt' => $alt,
          'thumbnail_url' => '',
        ];
        $video_info = \Drupal::getContainer()->get('pega_bolt_video.data')->getByVideoId($video_id);
        if ($target_id = $video_info['brightcove_entity']->thumbnail->target_id) {
          $file = File::load($target_id);
          if ($file instanceof FileInterface) {
            $video_data['thumbnail_url'] = $file->createFileUrl(FALSE);
          }
        }
      }
    }
    $this->list[0] = $this->createItem(0, $video_data);
  }

}
